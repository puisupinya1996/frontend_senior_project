import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Order from './components/Order';
import Process from './components/Process';
import ProcessEdit from './components/ProcessEdit';
import Header from './components/Header'
import OrderBilling from './components/OrderBilling'
import BillingEdit from './components/BillingEdit'
import Summary from './components/Summary'
import Product from './components/Product'
import User from './components/User'
import Department from './components/Department'
import * as serviceWorker from './serviceWorker';
import { Router, Route, Switch, Link } from 'react-router-dom';
import history from './history'
import Login from './components/login';

serviceWorker.unregister();

const renderAuthenRouter = () => {
    return (
        <Router history={history}>
            <div>
                <Header></Header>
            </div>
            <Switch>
                <Route path="/Order" component={Order} />
                <Route path="/Process/:poId" component={ProcessEdit} />
                <Route path="/Process" component={Process} />
                <Route path="/OrderBilling" component={OrderBilling} />
                <Route path="/BillingEdit/:poId" component={BillingEdit} />
                <Route path="/Summary" component={Summary} />
                <Route path="/Product" component={Product} />
                <Route path="/User" component={User} />
                <Route path="/Department" component={Department} />
            </Switch>
        </Router>
    )
}

const renderUnAuthenRouter = () => {
    return (
        <Router history={history}>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/" component={Login} />
            </Switch>
        </Router>
    )
}

const router = () => {
    if (localStorage.isLogin === "true") {
        return (
            renderAuthenRouter()
        )
    } else {
        return (
            renderUnAuthenRouter()
        )
    }

}

ReactDOM.render(router(), document.getElementById('root'));