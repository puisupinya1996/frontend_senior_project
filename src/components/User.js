import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import 'typeface-roboto';
import 'w3-css/w3.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class User extends Component {

   constructor(props) {
      super(props);
      this.state = {
         users: [],
         departments: [],
         userName: '',
         name: '',
         role: '',
         department: ''
      };
   }

   submit(id, username){
      confirmAlert({
        title: 'คุณต้องการลบผู้ใช้: ' + username,
        message: ' ',
        buttons: [
          {
            label: 'ใช่',
            onClick: () => this.deleteUser(id)
          },
          {
            label: 'ไม่ใช่'
          }
        ]
      });
    };

   handleUserNameChange(evt) {
      this.setState({
         userName: evt.target.value
      })
   }

   handleNameChange(evt) {
      this.setState({
         name: evt.target.value
      })
   }

   handleChangeRoleChange(evt) {
      this.setState({
         role: evt.target.value
      })
   }

   handleChangeDepartmentChange(evt) {
      console.log(evt.target.value);
      this.setState({
         department: evt.target.value
      })
   }

   componentDidMount() {
      fetch('http://localhost:8080/getAllUser')
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
               users: result
            });
         });

         fetch('http://localhost:8080/getAllDepartment')
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
               departments: result
            });
         });

   }

   callCreateUser() {

      let userDto = {
         userName: this.state.userName,
         name: this.state.name,
         role: this.state.role,
         department: this.state.department
      };

      let result = axios.post(
         'http://localhost:8080/createUser', userDto
      )
      result.then(
         window.location.reload()
      );
   }

   resetPassword(id) {
      axios.put('http://localhost:8080/resetPassword/' + id)
         .then(function (response) {
            // handle success
            // console.log(response);
         })
         .catch(function (error) {
            // handle error
            // console.log(error);
         })
         .then(function () {
            // always executed
         });
   }

   deleteUser(id) {
      axios.delete('http://localhost:8080/deleteUser/' + id)
         .then(function (response) {
            // handle success
            // console.log(response);
            window.location.reload();
         })
         .catch(function (error) {
            // handle error
            // console.log(error);
         })
         .then(function () {
            // always executed
            window.location.reload();
         });
   }

   render() {
      var department = this.state.departments.map((item, i) => {
         return (
            <option key={i} value={item.id}>{item.name}</option>
         )
      }, this);
      
      var data = this.state.users.map((user, i) => {
         return (
            //   <tr onClick={() => this.handleClick(user)}>
            <tr key={i}>
               <td>{user.row}</td>
               <td>{user.username}</td>
               <td>{user.role}</td>
               <td>{user.created}</td>
               <td>{user.name}</td>
               <td>{user.department}</td>
               <td className="w3-center"><Button variant="contained" onClick={() => this.resetPassword(user.id)}>Reset password</Button></td>
               <td className="w3-center"><Button variant="contained" onClick={() => this.submit(user.id, user.username)}>Delete</Button></td>
            </tr>
         )
      });

      return (
         <div className="w3-container">
            <div className="w3-row-padding w3-padding w3-margin-bottom">
            <h3>สร้างผู้ใช้งาน</h3>
               <div className="w3-row-padding">
                  <div className="w3-quarter">
                     <label>ชื่อเข้าใช้งานระบบ</label>
                     <input className="w3-input w3-border" type="text" placeholder="กรุณากรอก" onChange={this.handleUserNameChange.bind(this)} />
                  </div>
                  <div className="w3-quarter">
                     <label>ชื่อ - นามสกุล</label>
                     <input className="w3-input w3-border" type="text" placeholder="กรุณากรอก" onChange={this.handleNameChange.bind(this)} />
                  </div>
                  <div className="w3-quarter">
                     <label>สิทธิการใช้งาน</label>
                     <select className="w3-select" onChange={this.handleChangeRoleChange.bind(this)}>
                        <option value="">โปรดเลือก</option>
                        <option value="admin">admin</option>
                        <option value="plan">plan</option>
                        <option value="bill">bill</option>
                     </select>
                  </div>
                  <div className="w3-quarter">
                     <label>แผนก</label>
                     <select className="w3-select" onChange={this.handleChangeDepartmentChange.bind(this)}>
                        <option value="">โปรดเลือก</option>
                        <option value="0">ALL</option>
                       {department}
                     </select>
                  </div>
               </div>
               <div className="w3-padding w3-right">
                  <Button variant="contained" onClick={this.callCreateUser.bind(this)} >สร้างผู้ใช้งาน</Button>
               </div>
            </div>
            <h3>ตารางรายการผู้ใช้งานระบบ</h3>
            <table>
               <thead>
                  <tr>
                     <th>ลำดับที่</th>
                     <th>ชื่อเข้าใช้งานระบบ</th>
                     <th>สิทธิการใช้งาน</th>
                     <th>เวลาสร้าง</th>
                     <th>ชื่อ - นามสกุล</th>
                     <th>แผนก</th>
                     <th>รีเซ็ตรหัสผ่าน</th>
                     <th>ลบรายการ</th>
                  </tr>
               </thead>
               <tbody>
                  {data}
               </tbody>
            </table>
         </div>
      )
   }
}
export default User;