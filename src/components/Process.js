import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Button from '@material-ui/core/Button';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import 'w3-css/w3.css';
import { parseISO } from 'date-fns'
import history from '../history'
import NumberFormat from 'react-number-format';

class Process extends Component {

 
  constructor(props) {
    super(props);
    let array = localStorage.message.split("|");
    //  this.state.products = [];
    this.state = {
      startDate: new Date(),
      poNo: '',
      invoiceNo: '',
      departments: [],
      department: array[4]
    };
    this.state.products = [
      {
        id: 1,
        partNo: '',
        input: 0,
        output: 0
      }
    ];

  }

  componentDidMount() {
    let poId = this.props.match.params.poId;
    if (poId) {
      fetch('http://localhost:8080/getProcessByPo/' + poId)
        .then(response => {
          return response.json();
        }).then(result => {
          this.setState({
            products: result,
            poNo: result[0].poNo,
            invoiceNo: result[0].invoiceNo,
            startDate: parseISO(result[0].processDate)
          });
        });
    }

    fetch('http://localhost:8080/getAllDepartment')
      .then(response => {
        return response.json();
      }).then(result => {
        this.setState({
          departments: result
        });
      });
  }

  handlePoChange(evt) {
    this.setState({
      poNo: evt.target.value
    })
  }

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleRowDel(product) {
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      partNo: '',
      input: 0,
      output: 0
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  }

  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var products = this.state.products.slice();
    var newProducts = products.map(function (product) {

      for (var key in product) {
        if (key == item.name && product.id == item.id) {
          product[key] = item.value.replace(/,/g, '');
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
  };

  handleChangeDepartmentChange(evt) {
    this.setState({
      department: evt.target.value
    })
  }

  callCreateProcess() {
    let array = localStorage.message.split("|");
    let processDtos =
      this.state.products.map(p => (
        {
          poNo: this.state.poNo,
          partNo: p.partNo,
          input: p.input,
          output: p.output,
          userId: array[0],
          processDate: this.state.startDate,
          departmentId: this.state.department
        }))
      ;


    axios.post(
      'http://localhost:8080/process', processDtos
    )

  }

  callEditProcess() {
    let array = localStorage.message.split("|");
    let processDtos =
      this.state.products.map(p => (
        {
          id: p.id,
          poNo: this.state.poNo,
          partNo: p.partNo,
          input: p.input,
          output: p.output,
          userId: array[0],
          processDate: this.state.startDate,
        }))
      ;


    axios.post(
      'http://localhost:8080/editProcess', processDtos
    )

  }

  backToOrder() {
    history.push('/Order');
  }

  render() {
    let array = localStorage.message.split("|");
    const poid = this.props.match.params.poId;
    let showInvoice;
    if (poid) {
      showInvoice = <div><label>หมายเลขใบแจ้งหนี้</label>
        <input className="w3-input w3-border" type="text" placeholder="หมายเลขใบแจ้งหนี้" defaultValue={this.state.invoiceNo} />
      </div>
    }

    var department = this.state.departments.map((item, i) => {
      return (
        <option key={i} value={item.id}>{item.name}</option>
      )
    }, this);

    var showDepartment;
    if(array[4] === '0'){
      showDepartment = <select className="w3-select" onChange={this.handleChangeDepartmentChange.bind(this)}>
                          <option value="0">โปรดเลือก</option>
                          {department}
                        </select>
    } else {
      showDepartment = <select disabled className="w3-select" onChange={this.handleChangeDepartmentChange.bind(this)} value={array[4]}>
                        <option value="">โปรดเลือก</option>
                        {department}
                      </select>
    }

    return (
      <div className="w3-container">
        <div className="w3-row-padding w3-padding w3-margin-bottom">
          <h3>สร้างรายการสั่งซื้อ</h3>
          <div className="w3-quarter w3-padding">
            <label>แผนก</label>
            {showDepartment}
          </div>
          <ProductTable onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} 
          onRowDel={this.handleRowDel.bind(this)} products={this.state.products}  departmentId={this.state.department} />
          <div className="w3-row w3-right w3-padding">
            <label>หมายเลขใบสั่งซื้อ</label>
            <input className="w3-input w3-border" type="text" placeholder="กรุณากรอก" value={this.state.poNo} onChange={this.handlePoChange.bind(this)} />
            {showInvoice}
            <label>รายการของวันที่</label><br />
            <DatePicker
              title="กรุณาเลือกวันที่"
              dateFormat="dd/MM/yyyy"
              selected={this.state.startDate}
              onChange={this.handleChange}
            />
            <Button className="w3-margin-left" variant="contained" onClick={this.callCreateProcess.bind(this)} >Save</Button>
            <Button className="w3-margin-left" variant="contained" onClick={this.backToOrder.bind(this)} >Back</Button>

          </div>
        </div>
      </div>
    );

  }
}

class ProductTable extends React.Component {

  render() {
    var departmentId = this.props.departmentId;
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var rowDel = this.props.onRowDel;

    var product = this.props.products.map(function (product) {
      return (<ProductRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} key={product.id} departmentId={departmentId} />)
    });

    return (
      <div>
        <div className="w3-padding w3-right">
          <Button variant="contained" onClick={this.props.onRowAdd} className="btn btn-success pull-right">เพิ่มรายการ</Button>
        </div>
        <table>
          <thead>
            <tr>
              <th>Part No.</th>
              <th>Input</th>
              <th>Output</th>
              <th>ลบแถว</th>
            </tr>
          </thead>

          <tbody>
            {product}

          </tbody>

        </table>
      </div>
    );
  }
}


class ProductRow extends React.Component {
  onDelEvent() {
    this.props.onDelEvent(this.props.product);

  }
  render() {
    var departmentId = this.props.departmentId;
    return (
      <tr className="eachRow">
        <EditableSelectCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "partNo",
          value: this.props.product.partNo,
          id: this.props.product.id
        }}
        departmentId={departmentId}
        />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "input",
          value: this.props.product.input,
          id: this.props.product.id
        }} />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "output",
          value: this.props.product.output,
          id: this.props.product.id
        }} />
        <td className="del-cell">
          <input type="button" onClick={this.onDelEvent.bind(this)} value="X" className="del-btn" />
        </td>
      </tr>
    );

  }

}
class EditableCell extends React.Component {

  render() {
    return (
      <td >
        <NumberFormat className="w3-input w3-border" thousandSeparator={true} name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate} />
      </td>
    );

  }

}

class EditableSelectCell extends React.Component {

  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {
      products: []
    }
  }

  //  departmentId = this.props.departmentId;

  componentDidMount() {
    var res = localStorage.message.split("|");
    fetch('http://localhost:8080/getProductByDepartmentId/' + Number(res[4]))
      .then(response => {
        return response.json();
      }).then(result => {
        console.log(result);
        this.setState({
          products: result
        });
      });
  }

  render() {
    
    var product = this.state.products.map((item, i) => {
      return (
        <option key={i} value={item.partNo}>{item.partNo}</option>
      )
    }, this);
    return (
      <td >
        <select className="w3-select" onChange={this.props.onProductTableUpdate}
          name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value}>
          <option value="">โปรดเลือก</option>
          {product}
        </select>

      </td>
    );

  }

}

export default Process;