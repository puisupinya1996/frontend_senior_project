import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Button from '@material-ui/core/Button';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import 'w3-css/w3.css';
import { parseISO } from 'date-fns'
import history from '../history'
import NumberFormat from 'react-number-format';

class BillingEdit extends Component {


  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {
      startDate: new Date(),
      poNo: '',
      invoiceNo: '',
      amount: '',
      vat: '',
      totalAmount: ''
    };
    this.state.products = [
      {
        id: 1,
        partNo: '',
        input: 0,
        output: 0,
        unitPrice: 0,
        netAmount: 0
      }
    ];

  }

  componentDidMount() {
    let poId = this.props.match.params.poId;
    if (poId) {
      fetch('http://localhost:8080/getProcessByPo/' + poId)
        .then(response => {
          return response.json();
        }).then(result => {
          this.setState({
            products: result,
            poId: result[0].poId,
            poNo: result[0].poNo,
            invoiceNo: result[0].invoiceNo,
            startDate: parseISO(result[0].processDate)
          });
          let amount = 0;

          for (var i = 0; i < result.length; i++) {
            amount += result[i].netAmount;
          }
          this.setState({
            amount: amount,
            vat: (amount * 7) / 100,
            totalAmount: amount + (amount * 7) / 100
          });

        });
    }
  }

  handlePoChange(evt) {
    this.setState({
      poNo: evt.target.value
    })
  }

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleRowDel(product) {
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      partNo: '',
      input: 0,
      output: 0,
      unitPrice: 0,
      netAmount: 0
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  }

  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var products = this.state.products.slice();
    let amount = 0;
    var newProducts = products.map(function (product) {

      for (var key in product) {
        if (key == item.name && product.id == item.id) {
          product[key] = item.value.replace(/,/g, '');
          if (item.name === 'unitPrice') {
            product['netAmount'] = product[key] * product['output'];
          }
        }
      }
      amount = amount + product['netAmount']
      return product;
    });

    this.setState({
      products: newProducts,
      amount: amount,
      vat: (amount * 7) / 100,
      totalAmount: amount + (amount * 7) / 100
    });
  };

  callEditProcess() {
    let array = localStorage.message.split("|");
    let processDtos =
      this.state.products.map(p => (
        {
          id: p.id,
          poNo: this.state.poNo,
          partNo: p.partNo,
          input: p.input,
          unitPrice: p.unitPrice,
          netAmount: p.netAmount,
          output: p.output,
          userId: array[0],
          processDate: this.state.startDate,
        }))
      ;

    axios.post(
      'http://localhost:8080/editProcess', processDtos
    ).then(response => {

      axios(`http://localhost:8080/generateBill/` + this.state.poId, {
        method: 'GET',
        responseType: 'blob' //Force to receive data in a Blob Format
      })
        .then(response => {
          const file = new Blob(
            [response.data],
            { type: 'application/pdf' });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        })
        .catch(error => {
          console.log(error);
        });

    }).catch(error => {
      console.log(error);
    });

  }

  backToOrder() {
    history.push('/OrderBilling');
  }

  render() {
    const poid = this.props.match.params.poId;
    let showInvoice;
    if (poid) {
      showInvoice = <div><label>หมายเลขใบแจ้งหนี้</label>
        <input disabled className="w3-input w3-border" type="text" placeholder="หมายเลขใบแจ้งหนี้" defaultValue={this.state.invoiceNo} />
      </div>
    }
    return (
      <div className="w3-container">
      <div className=" w3-padding w3-margin-bottom">
         <h3>รายการใบวางบิล</h3>

        <ProductTable onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} />
        <div className="w3-row w3-right w3-padding">
        <label>จำนวนเงิน</label>
        <NumberFormat disabled thousandSeparator={true} className="w3-input w3-border" value={this.state.amount} />
        <label>ภาษี</label>
        <NumberFormat disabled thousandSeparator={true} className="w3-input w3-border" value={this.state.vat} />
        <label>ราคารวมภาษี</label>
        <NumberFormat disabled thousandSeparator={true} className="w3-input w3-border" value={this.state.totalAmount} />


        <label>หมายเลขใบสั่งซื้อ</label>
        <input disabled className="w3-input w3-border" type="text" placeholder="หมายเลขใบสั่งซื้อ" value={this.state.poNo} onChange={this.handlePoChange.bind(this)} />
        {showInvoice}
        <label>รายการของวันที่</label><br/>
        <DatePicker disabled
          dateFormat="dd/MM/yyyy"
          selected={this.state.startDate}
          onChange={this.handleChange}
        />
        <Button variant="contained" onClick={this.callEditProcess.bind(this)} >Save/Print</Button>
        <Button variant="contained" onClick={this.backToOrder.bind(this)} >Back</Button>
        </div>
      </div>
      </div>
    );

  }
}

class ProductTable extends React.Component {

  render() {
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var rowDel = this.props.onRowDel;
    var product = this.props.products.map(function (product) {
      return (<ProductRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} key={product.id} />)
    });
    return (
      <div>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Part No.</th>
              <th>Input</th>
              <th>Output</th>
              <th>Unit Price</th>
              <th>Net Amount</th>
            </tr>
          </thead>
          <tbody>
            {product}

          </tbody>

        </table>
      </div>
    );
  }
}


class ProductRow extends React.Component {
  onDelEvent() {
    this.props.onDelEvent(this.props.product);

  }
  render() {

    return (
      <tr className="eachRow">
        <EditableSelectCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "partNo",
          value: this.props.product.partNo,
          id: this.props.product.id
        }} />
        <DisableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "input",
          value: this.props.product.input,
          id: this.props.product.id
        }} />
        <DisableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "output",
          value: this.props.product.output,
          id: this.props.product.id
        }} />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "unitPrice",
          value: this.props.product.unitPrice,
          id: this.props.product.id
        }} />
        <DisableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "netAmount",
          value: this.props.product.netAmount,
          id: this.props.product.id
        }} />
      </tr>
    );

  }

}
class EditableCell extends React.Component {

  render() {
    return (
      <td>
        <NumberFormat className="w3-input w3-border" thousandSeparator={true} name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate} />
      </td>
    );

  }

}

class DisableCell extends React.Component {

  render() {
    return (
      <td>
        <NumberFormat disabled className="w3-input w3-border" thousandSeparator={true} name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate} />
      </td>
    );

  }

}

class EditableSelectCell extends React.Component {

  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {
      products:[]
    }
  }

  componentDidMount() {
    var res = localStorage.message.split("|");
      fetch('http://localhost:8080/getProductByDepartmentId/' + Number(res[4]))
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
              products: result
            });
         });
   }

  render() {
    var product = this.state.products.map((item, i) => {
      return (
         <option key={i} value={item.partNo}>{item.partNo}</option>
      )
   }, this);
    return (
      <td>
        <select disabled className="w3-select" onChange={this.props.onProductTableUpdate}
          name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value}>
          <option value="">โปรดเลือก</option>
          {product}
        </select>

      </td>
    );

  }

}

export default BillingEdit;