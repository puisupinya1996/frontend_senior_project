import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Button from '@material-ui/core/Button';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import 'w3-css/w3.css';
import { parseISO } from 'date-fns'
import history from '../history'
import NumberFormat from 'react-number-format';

class ProcessEdit extends Component {


  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {
      startDate: new Date(),
      poNo: '',
      invoiceNo: ''
    };
    this.state.products = [
      {
        id: 1,
        partNo: '',
        input: 0,
        output: 0
      }
    ];

  }

  componentDidMount() {
    let poId = this.props.match.params.poId;
    if (poId) {
      fetch('http://localhost:8080/getProcessByPo/' + poId)
        .then(response => {
          return response.json();
        }).then(result => {
          this.setState({
            products: result,
            poNo: result[0].poNo,
            invoiceNo: result[0].invoiceNo,
            startDate: parseISO(result[0].processDate)
          });
        });
    }
  }

  handlePoChange(evt) {
    this.setState({
      poNo: evt.target.value
    })
  }

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleRowDel(product) {
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      partNo: '',
      input: 0,
      output: 0
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  }

  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var products = this.state.products.slice();
    var newProducts = products.map(function (product) {

      for (var key in product) {
        if (key == item.name && product.id == item.id) {
          product[key] = item.value.replace(/,/g, '');
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
  };

  callCreateProcess() {
    let array = localStorage.message.split("|");
    let processDtos =
      this.state.products.map(p => (
        {
          poNo: this.state.poNo,
          partNo: p.partNo,
          input: p.input,
          output: p.output,
          userId: array[0],
          processDate: this.state.startDate,
          departmentId: array[4]
        }))
      ;


    axios.post(
      'http://localhost:8080/process', processDtos
    )

  }

  callEditProcess() {
    let array = localStorage.message.split("|");
    let processDtos =
      this.state.products.map(p => (
        {
          id:p.id,
          poNo: this.state.poNo,
          partNo: p.partNo,
          input: p.input,
          output: p.output,
          userId: array[0],
          processDate: this.state.startDate,
        }))
      ;


    axios.post(
      'http://localhost:8080/editProcess', processDtos
    )
  }

  backToOrder() {
    history.push('/Order');
}

  render() {
    const poid = this.props.match.params.poId;
    let showInvoice;
    if (poid) {
      showInvoice = <div><label>หมายเลขใบแจ้งหนี้</label>
        <input className="w3-input w3-border" type="text" placeholder="หมายเลขใบแจ้งหนี้" defaultValue={this.state.invoiceNo} disabled/>
      </div>
    }
    return (
      <div className="w3-container">
        <ProductTable onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} />
        <div class="w3-row w3-right w3-padding">
        <label>หมายเลขใบสั่งซื้อ</label>
        <input className="w3-input w3-border" type="text" placeholder="หมายเลขใบสั่งซื้อ" value={this.state.poNo} onChange={this.handlePoChange.bind(this)} disabled/>
        {showInvoice}
        <label>รายการของวันที่</label><br/>
        <DatePicker disabled
        dateFormat="dd/MM/yyyy"
          selected={this.state.startDate}
          onChange={this.handleChange}
        />
        <Button variant="contained" onClick={this.callEditProcess.bind(this)} >Save Edit</Button>
        <Button variant="contained" onClick={this.backToOrder.bind(this)} >Back</Button>
        </div>
      </div>
    );

  }
}

class ProductTable extends React.Component {

  render() {
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var rowDel = this.props.onRowDel;
    var product = this.props.products.map(function (product) {
      return (<ProductRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} key={product.id} />)
    });
    return (
      <div className="w3-container">
      <div className="w3-row-padding w3-padding w3-margin-bottom">
        <h3>แก้ไขรายการสั่งซื้อ</h3>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>ลำดับที่</th>
              <th>Part No.</th>
              <th>Input</th>
              <th>Output</th>
            </tr>
          </thead>

          <tbody>
            {product}

          </tbody>

        </table>
      </div>
      </div>
    );
  }
}


class ProductRow extends React.Component {
  onDelEvent() {
    this.props.onDelEvent(this.props.product);

  }
  render() {

    return (
      <tr className="eachRow">
        <td>
        {this.props.product.row}
      </td>
        <EditableSelectCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "partNo",
          value: this.props.product.partNo,
          id: this.props.product.id
        }} />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "input",
          value: this.props.product.input,
          id: this.props.product.id
        }} />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "output",
          value: this.props.product.output,
          id: this.props.product.id
        }} />
 
      </tr>
    );

  }

}
class EditableCell extends React.Component {

  render() {
    return (
      <td>
        <NumberFormat className="w3-input w3-border" thousandSeparator={true} name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate} />
      </td>
    );

  }

}

class EditableSelectCell extends React.Component {

  constructor(props) {
    super(props);

    //  this.state.products = [];
    this.state = {
      products:[]
    }
  }

  componentDidMount() {
    var res = localStorage.message.split("|");
      fetch('http://localhost:8080/getProductByDepartmentId/' + Number(res[4]))
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
              products: result
            });
         });
   }

  render() {
    var product = this.state.products.map((item, i) => {
      return (
         <option key={i} value={item.partNo}>{item.partNo}</option>
      )
   }, this);
    return (
      <td>
        <select disabled className="w3-select" onChange={this.props.onProductTableUpdate}
          name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value}>
          <option value="">โปรดเลือก</option>
          {product}
        </select>

      </td>
    );

  }

}

export default ProcessEdit;