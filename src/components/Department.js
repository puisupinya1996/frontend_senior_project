import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import 'typeface-roboto';
import 'w3-css/w3.css';
import './header.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class Department extends Component {

   constructor(props) {
      super(props);
      this.state = {
         departments: [],
         name: ''
      };
   }

   submit(id, username){
      confirmAlert({
        title: 'คุณต้องการลบแผนก: ' + username,
        message: ' ',
        buttons: [
          {
            label: 'ใช่',
            onClick: () => this.deleteDepartment(id)
          },
          {
            label: 'ไม่ใช่'
          }
        ]
      });
    };

   handleNameChange(evt) {
      this.setState({
         name: evt.target.value
      })
   }

   componentDidMount() {
      fetch('http://localhost:8080/getAllDepartment')
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
               departments: result
            });
         });
   }

   callCreateDepartment() {

      let departmentDto = {
         name: this.state.name,
      };

      let result = axios.post(
         'http://localhost:8080/createDepartment', departmentDto
      )
      result.then(
         window.location.reload()
      );
   }

   deleteDepartment(id) {
      axios.delete('http://localhost:8080/deleteDepartment/' + id)
         .then(function (response) {
            window.location.reload();
         })
         .catch(function (error) {
         })
         .then(function () {
            window.location.reload();
         });
   }

   render() {
      var data = this.state.departments.map((department, i) => {
         return (
            //   <tr onClick={() => this.handleClick(user)}>
            <tr key={i}>
               <td>{department.id}</td>
               <td>{department.name}</td>
               <td className="w3-center"><Button variant="contained" onClick={() => this. submit(department.id, department.name)}>Delete</Button></td>
            </tr>
         )
      });

      return (
         <div className="w3-container">
            <div className="w3-row-padding w3-padding w3-margin-bottom">
            <h3>สร้างแผนก</h3>
               <div className="w3-row-padding">
                  <div className="w3-quarter">
                     <label>ชื่อแผนก</label>
                     <input className="w3-input w3-border" type="text" placeholder="กรุณากรอกชื่อแผนก" onChange={this.handleNameChange.bind(this)} />
                  </div>
                  <div className="pt-24">
                  <Button variant="contained" onClick={this.callCreateDepartment.bind(this)} >สร้างแผนก</Button>
                  </div>
               </div>
            </div>
            <h3>ตารางรายการแผนก</h3>
            <table>
               <thead>
                  <tr>
                     <th>ลำดับที่</th>
                     <th>ชื่อแผนก</th>
                     <th>ลบรายการ</th>
                  </tr>
               </thead>
               <tbody>
                  {data}
               </tbody>
            </table>
         </div>
      )
   }
}
export default Department;