import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import 'typeface-roboto';
import 'w3-css/w3.css';
import history from '../history'
import JwPagination from 'jw-react-pagination';

class OrderBilling extends Component {

   constructor(props) {
      super(props);
      this.state = {
         poList: [],
         pageOfItems: []
      };
      this.onChangePage = this.onChangePage.bind(this);
   }

   onChangePage(pageOfItems) {
      // update state with new page of items
      this.setState({ pageOfItems: pageOfItems });
     
  }

   componentDidMount() {
    var res = localStorage.message.split("|");
      fetch('http://localhost:8080/getPoByDepartment/' + Number(res[4]))
         .then(response => {
            return response.json();
         }).then(result => {
            this.setState({
                poList: result
            });
         });
   }

   editPoDetail(id) {
      history.push('/BillingEdit/' + id);
 }

 deletePo(id) {
   axios.delete('http://localhost:8080/deletePo/' + id)
      .then(function (response) {
         // handle success
         // console.log(response);
         window.location.reload();
      })
      .catch(function (error) {
         // handle error
         // console.log(error);
      })
      .then(function () {
         // always executed
      });
}

 popage(){
    history.push('/Process');
 }

   render() {
      var data = this.state.pageOfItems.map((po, i) => {
         return (
            <tr key={i}>
               <td>{po.row}</td>
               <td>{po.poNo}</td>
               <td>{po.invoiceNo}</td>
               <td>{po.processDate}</td>
               <td>{po.createName}</td>
               <td>{po.createTime}</td>
               <td>{po.editName}</td>
               <td>{po.editTime}</td>
               <td className="w3-center"><Button variant="contained" onClick={() => this.editPoDetail(po.poId)}>Enter</Button></td>
              </tr>
         )
      });

      return (
         <div className="w3-container">
        
            <h3>ตารางรายการใบวางบิล</h3>
            <table>
               <thead>
                  <tr>
                     <th>ลำดับที่</th>
                     <th>หมายเลขใบสั่งซื้อ</th>
                     <th>หมายเลขใบแจ้งหนี้</th>
                     <th>วันที่ทำรายการ</th>
                     <th>ผู้ทำรายการ</th>
                     <th>สร้างรายการเวลา</th>
                     <th>ผู้แก้ไขรายการ</th>
                     <th>แก้ไขรายการเวลา</th>
                     <th>วางบิล</th>
                  </tr>
               </thead>
               <tbody>
                  {data}
               </tbody>
            </table>
            <div className=" w3-right">
                <JwPagination items={this.state.poList} onChangePage={this.onChangePage} pageSize={4}/>
                    
                    </div> 
         </div>
      )
   }
}
export default OrderBilling;