import React, { Component } from "react";
import history from '../history';
import './header.css';
import { NavLink } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import pdf from './manual.pdf'


class Header extends Component {

  submit = () => {
    confirmAlert({
      title: 'คุณต้องการออกจากระบบหรือไม่ ?',
      message: ' ',
      buttons: [
        {
          label: 'ใช่',
          onClick: () => this.callLogOut()
        },
        {
          label: 'ไม่ใช่',
          // onClick: () => onClose()
        }
      ]
    });
  };

  callLogOut() {
    localStorage.clear();
    history.push('/');
    window.location.reload();
  }

  render() {
    var name =
      console.log(localStorage.message);
    var res = localStorage.message.split("|");
    var name = res[1];
    if (localStorage.message.includes("admin")) {
      return (
        <div>
          <nav className="navbar">
            <NavLink
              exact
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/Order"
            >
              นำเข้า-ออกชิ้นงาน
    </NavLink>
            <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/OrderBilling"
            >
              วางบิล
    </NavLink>
            <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/Summary"
            >
              รายงานสรุป
    </NavLink>
            <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/Product"
            >
              จัดการชิ้นงาน
    </NavLink>
            <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/Department"
            >
              จัดการแผนก
    </NavLink>
            <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/User"
            >
              ตั้งค่าผู้ใช้งาน
    </NavLink>
            <a href={pdf} target="_blank">คู่มือการใช้งาน</a>
            <p>สวัสดีคุณ : {name}</p>
            <Icon className="icon" id="btnLogout" onClick={this.submit}>exit_to_app</Icon>
          </nav>
        </div>
        // <Grid container spacing={3}>
        //   <Grid item xs={12}>
        //     <ButtonGroup variant="contained" fullWidth aria-label="full width outlined button group">
        //       <Button onClick={() => history.push('/Order')}>นำเข้า-ออกชิ้นงาน</Button>
        //       <Button onClick={() => history.push('/OrderBilling')}>วางบิล</Button>
        //       <Button onClick={() => history.push('/Summary')}>รายงานสรุป</Button>
        //       <Button onClick={() => history.push('/Product')}>จัดการชิ้นงาน</Button>
        //       <Button onClick={() => history.push('/Department')}>จัดการแผนก</Button>
        //       <Button onClick={() => history.push('/User')}>ตั้งค่าผู้ใช้งาน</Button>
        //       <Button onClick={this.callLogOut}>ออกจากระบบ</Button>
        //       {/* <Button onClick={()=>history.push('/')}>ออกจากระบบ</Button> */}

        //     </ButtonGroup>
        //   </Grid>
        //         <div>ยินดีต้อนรับคุณ {res[1]}</div>
        // </Grid>
      )
    } else if (localStorage.message.includes("plan")) {
      return (
        <div>
          <nav className="navbar">
            <NavLink
              exact
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/Order"
            >
              นำเข้า-ออกชิ้นงาน
    </NavLink>
    <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/OrderBilling"
            >
              วางบิล
    </NavLink>
    <a href={pdf} target="_blank">คู่มือการใช้งาน</a>
            <p>สวัสดีคุณ : {name}</p>
            <Icon className="icon" id="btnLogout" onClick={this.submit}>exit_to_app</Icon>
          </nav>
        </div>
      )
    } else if (localStorage.message.includes("bill")) {
      return (
        <div>
          <nav className="navbar">
          <NavLink
              activeClassName="navbar__link--active"
              className="navbar__link"
              to="/OrderBilling"
            >
              วางบิล
    </NavLink>
    
    <a href={pdf} target="_blank">คู่มือการใช้งาน</a>
            <p>สวัสดีคุณ : {name}</p>
            <Icon className="icon" id="btnLogout" onClick={this.submit}>exit</Icon>
          </nav>
        </div>
      )
    }
  }
}

export default Header;
