import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'typeface-roboto';
import 'w3-css/w3.css';
import moment from 'moment';
import JwPagination from 'jw-react-pagination';
import NumberFormat from 'react-number-format';

class Summary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            summarys: [],
            startDate: new Date(),
            endDate: new Date(),
            departmentId: '',
            departments: [],
            pageOfItems: []
        };
        this.onChangePage = this.onChangePage.bind(this);
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
       
    }

    componentDidMount() {
        fetch('http://localhost:8080/getAllDepartment')
            .then(response => {
                return response.json();
            }).then(result => {
                this.setState({
                    departments: result
                });
            });

    }

    handleChangeDepartmentChange(evt) {
        this.setState({
            departmentId: evt.target.value
        })
    }

    // componentDidMount() {
    //     fetch('http://localhost:8080/getAllUser')
    //         .then(response => {
    //             return response.json();
    //         }).then(result => {
    //             this.setState({
    //                 users: result
    //             });
    //         });
    // }

    getProcess() {
        let start = this.state.startDate;
        let end = this.state.endDate;
        fetch('http://localhost:8080/getBillingProcess/' + this.state.departmentId + '/' + moment(start).format('LL') + '/' + moment(end).format('LL'))
            .then(response => {
                return response.json();
            }).then(result => {
                this.setState({
                    summarys: result
                });
            });
    }





    handleStartDateChange = date => {
        this.setState({
            startDate: date
        });
    };

    handleEndDateChange = date => {
        this.setState({
            endDate: date
        });
    };

    render() {
        var department = this.state.departments.map((item, i) => {
            return (
                <option key={i} value={item.id}>{item.name}</option>
            )
        }, this);

        var sub = this.state.pageOfItems.map((summary, i) => {
            return (
                summary.classList.map((item, i) => {
                    return (
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{item.partNo}</td>
                            <td>
                            <NumberFormat value={item.input} displayType={'text'} thousandSeparator={true}/>
                            </td> 
                            <td>
                            <NumberFormat value={item.output} displayType={'text'} thousandSeparator={true}/>
                            </td>
                            <td>
                            <NumberFormat value={item.unitPrice} displayType={'text'} thousandSeparator={true}/>
                            </td>
                            <td>
                            <NumberFormat value={item.netAmount} displayType={'text'} thousandSeparator={true}/>
                            </td>
                        </tr>
                    )
                })
            )
        });

        var data = this.state.pageOfItems.map((summary, i) => {
            return (
                //   <tr onClick={() => this.handleClick(user)}>
                <tr key={i}>
                    <td>{summary.row}</td>
                    <td>{summary.poNo}</td>
                    <td>{summary.invoiceNo}</td>
                    <td>{summary.processDate}</td>
                    <td> <table>
                        <thead>
                            <tr>
                                <th>ลำดับที่</th>
                                <th>partNo</th>
                                <th>input</th>
                                <th>out put</th>
                                <th>unitPrice</th>
                                <th>netAmount</th>
                            </tr>
                        </thead>
                        <tbody>{sub[i]}</tbody>
                    </table>
                    </td>
                </tr>
            )
        });

        return (
            <div className="w3-container">
                <div className="w3-row-padding w3-padding w3-margin-bottom">
                    <h3>สรุปผล</h3>
                    <div className="w3-row-padding">
                        <div className="w3-third">
                            <label>แผนก</label>
                            <select className="w3-select" onChange={this.handleChangeDepartmentChange.bind(this)}>
                                <option value="">โปรดเลือก</option>
                                {department}
                            </select>
                        </div>
                        <div className="w3-third">
                            <div className="w3-half"><label>ตั้งแต่วันที่</label><br/>
                            <DatePicker
                                dateFormat="dd/MM/yyyy"
                                selected={this.state.startDate}
                                onChange={this.handleStartDateChange}
                            /></div>
                                                    <div className="w3-half">
                            <label>ถึงวันที่</label><br/>
                            <DatePicker
                                dateFormat="dd/MM/yyyy"
                                selected={this.state.endDate}
                                onChange={this.handleEndDateChange}
                            />
                        </div>
                        </div>
                        <div className="w3-third w3-margin-top">
                        <Button variant="contained" onClick={this.getProcess.bind(this)} >เรียกดูรายงาน</Button>
                            </div>
                    </div>

                </div>
                <h3>ตารางรายการสรุปผล</h3>
                <table>
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>poNo</th>
                            <th>invoiceNo</th>
                            <th>processDate</th>
                            <th>process</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data}
                    </tbody>
                </table>
                <div className=" w3-right">
                <JwPagination items={this.state.summarys} onChangePage={this.onChangePage} pageSize={2}/>
                    
                    </div> 
            </div>
        )
    }
}
export default Summary;