import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Button from '@material-ui/core/Button';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import moment from 'moment';
import Checkbox from '@material-ui/core/Checkbox';

class Billing extends Component {

    constructor(props) {
        super(props);

        //  this.state.products = [];
        this.state = {
            billing: [
                {
                    partNo: '',
                    input: 0,
                    output: 0,
                    unitCost: 0
                }],
            startDate: new Date()
        };
    }

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    searchProcess() {
        let main = this.state.startDate
        fetch('http://localhost:8080/getBillingProcess/' + moment(main).format('LL'))
            .then(response => {
                return response.json();
            }).then(result => {
                this.setState({
                    billing: result
                });
            });
        // let main = this.state.startDate
        // axios.get('http://localhost:8080/getBillingProcess/' + moment(main).format('LL'))
        //     .then(function (response) {
        //         console.log(response)
        //         this.setState({
        //             billing: [response.data]
        //          });
        //          console.log(this.state.billing)
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         // console.log(error);
        //     })
        //     .then(function () {
        //         // always executed
        //     });
    }
exportbillingPdf(){
    let main = this.state.startDate
    axios(`http://localhost:8080/generateBill/`+ moment(main).format('LL'), {
        method: 'GET',
        responseType: 'blob' //Force to receive data in a Blob Format
    })
    .then(response => {
        const file = new Blob(
          [response.data], 
          {type: 'application/pdf'});
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
    })
    .catch(error => {
        console.log(error);
    });
    }

    render() {

        var data = this.state.billing.map((bill, i) => {
            return (
                <tr key={i}>
                    <td>{bill.partNo}</td>
                    <td>{bill.input}</td>
                    <td>{bill.output}</td>
                    <td><input type='text' /></td>
                    <td></td>
                    <td>
                        <Checkbox
                            defaultChecked
                            color="default"
                            value="checkedG"
                            inputProps={{
                                'aria-label': 'checkbox with default color',
                            }}
                        />
                    </td>
                </tr>
            )
        });

        return (
            <div className="w3-container">
                <DatePicker
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                />
                <Button variant="contained" onClick={this.searchProcess.bind(this)}>ค้นหารายการ</Button>
                <div>
                    <h3>ตารางรายการออกใบเสร็จ</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>Part No.</th>
                                <th>Input</th>
                                <th>Output</th>
                                <th>ราคาต่อหน่วย</th>
                                <th>รวม</th>
                                <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data}
                        </tbody>
                    </table>

                </div>
                <Button variant="contained"  onClick={this.exportbillingPdf.bind(this)}>บันทึกรายการและออกใบเสร็จ</Button>
            </div>
        );

    }
}

export default Billing;