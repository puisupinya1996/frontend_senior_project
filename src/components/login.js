import React, { Component } from 'react';
import axios from 'axios';
import history from '../history'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import '../index.css';

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userName: "",
      password: ""
    }

  }
  handleUserNameChange(evt) {
    this.setState({
      userName: evt.target.value
    })
  }

  handlePasswordChange(evt) {
    this.setState({
      password: evt.target.value
    })
  }

  handleResult(res) {
    console.log(res.data);
    if (res.data.httpStatus === 'OK') {
      localStorage.message = res.data.message;
      localStorage.isLogin = true;
      if (localStorage.message.includes("admin")) {
        history.push('/Order');
      }else if (localStorage.message.includes("plan")) {
        history.push('/Order');
      } else if (localStorage.message.includes("bill")) {
        history.push('/OrderBilling');
      }
      window.location.reload();
    } else {
      localStorage.isLogin = false;
    }
  }

  callLogin() {

    let loginDto = {
      userName: this.state.userName,
      password: this.state.password
    };

    let result = axios.post(
      'http://localhost:8080/login', loginDto
    )
    result.then(this.handleResult);
  }

  render() {
    return (
      <div><div className="w3-margin-top"> </div>
        <div className="w3-margin-top"></div>
        <div className="w3-display-topmiddle w3-xxxlarge w3-margin-top">ระบบวางบิล</div>
      <div className="login">
        <div>
          <TextField
            id="userName"
            label="Username"
            placeholder="Enter Username"           
            required
            onChange={this.handleUserNameChange.bind(this)} />
        </div>
        <div className="pb-20">
          <TextField
            id="password"
            label="Password"
            placeholder="Enter Username"
            type="password"
            required
            onChange={this.handlePasswordChange.bind(this)} 
          />
        </div>
        <div>
          <Button id="btnLogin" variant="contained" onClick={this.callLogin.bind(this)} >Login</Button>
        </div>
      </div>
      </div>
    );
  }
}

export default Login;
