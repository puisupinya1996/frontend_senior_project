import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import 'typeface-roboto';
import 'w3-css/w3.css';
import Dropzone from 'react-dropzone'
import JwPagination from 'jw-react-pagination';
import './header.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null,
            products: [],
            departments: [],
            department:'',
            pageOfItems: []
        };
        this.onChangePage = this.onChangePage.bind(this);
    }

    submit(id, username){
        confirmAlert({
          title: 'คุณต้องการลบชิ้นงาน: ' + username,
          message: ' ',
          buttons: [
            {
              label: 'ใช่',
              onClick: () => this.deleteProduct(id)
            },
            {
              label: 'ไม่ใช่'
            }
          ]
        });
      };

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
       
    }

    componentDidMount() {
        fetch('http://localhost:8080/getAllDepartment')
            .then(response => {
                return response.json();
            }).then(result => {
                this.setState({
                    departments: result
                });
            });

        fetch('http://localhost:8080/getAllProduct')
            .then(response => {
                return response.json();
            }).then(result => {
                this.setState({
                    products: result
                });
            });
    }

    callCreateUser() {

        let userDto = {
            userName: this.state.userName,
            name: this.state.name,
            role: this.state.role,
            department: this.state.department
        };

        let result = axios.post(
            'http://localhost:8080/createUser', userDto
        )
        result.then(
            window.location.reload()
        );
    }

    deleteProduct(id) {
        axios.delete('http://localhost:8080/deleteProduct/' + id)
            .then(function (response) {
                window.location.reload();
            })
            .catch(function (error) {

            })
            .then(function () {
                window.location.reload();
            });
    }

    handleChangeDepartmentChange(evt) {
        console.log(evt.target.value);
        this.setState({
            department: evt.target.value
        })
    }

    onDrop = (files) => {

        const config = { headers: { 'Content-Type': false } };
        let fd = new FormData();

        files.map((file) => {
            fd.append('file', file);
            fd.append('departmentId', this.state.department);
        });

        axios.post(`http://localhost:8080/uploadFile`, fd, config)
            .then((response) => {
                // callback(response);
                window.location.reload();
            })
            .catch(error => {
                // errorResponse(error);
            })
        // var formData = new FormData();

        // files.map((file, index) => {
        //   formData.append(`file${index}`, file);
        // });

        // fetch('http://localhost:8080/uploadFile', {
        //   // content-type header should not be specified!
        //   method: 'POST',
        //   body: formData,
        // })
        //   .then(response => response.json())
        //   .then(success => {
        //     // Do something with the successful response
        //   })
        //   .catch(error => console.log(error)
        // );
    }

    render() {
        var department = this.state.departments.map((item, i) => {
            return (
                <option key={i} value={item.id}>{item.name}</option>
            )
        }, this);

        var data = this.state.pageOfItems.map((product, i) => {
            return (
                //   <tr onClick={() => this.handleClick(user)}>
                <tr key={i}>
                    <td>{product.id}</td>
                    <td>{product.partNo}</td>
                    <td>{product.departmentName}</td>
                    <th><img src={`data:image/jpeg;base64,${product.imageBase64}`} /></th>
                    <td className="w3-center"><Button variant="contained" onClick={() => this.submit(product.id, product.partNo)}>Delete</Button></td>
                </tr>
            )
        });

        return (
            <div className="w3-container">
                
                <div className="w3-row-padding w3-padding w3-margin-bottom">
                <h3>สร้างชิ้นงาน</h3>
                <div className="w3-quarter">
                    <label>แผนก</label>
                    <select className="w3-select" onChange={this.handleChangeDepartmentChange.bind(this)}>
                        <option value="">โปรดเลือก</option>
                        {department}
                    </select>
                </div>
                    <div className="w3-quarter">
                    <label>อัพโหลดไฟล์</label>
                        <Dropzone onDrop={this.onDrop}>
                            {({ getRootProps, getInputProps }) => (
                                <div  className="dropzone" {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    Click me to upload a file!
                                </div>
                            )}
                        </Dropzone>
                    </div>
                   
                </div>

                <h3>ตารางรายการชิ้นงาน</h3>
                <table>
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>Part No.</th>
                            <th>แผนก</th>
                            <th>รูปชิ้นงาน</th>
                            <th>ลบรายการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data}
                    </tbody>
                </table>
                <div className=" w3-right">
                <JwPagination items={this.state.products} onChangePage={this.onChangePage} pageSize={2}/>
                    
                    </div> 
            </div>
            
        )
    }
}
export default Product;